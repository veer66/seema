# Seema
Khmer, Lao, Myanmar, and Thai word segmentation/breaking library in Python written in Rust

# Example

```
>>> from seema import Seema
>>> s = Seema("mixed-wordlist.txt", "mixed-cluster-rules.txt")
>>> s.segment_into_strings("វេវចនានុក្រមពហុភាសាដោយឥតគិតថ្លៃฉันกินข้าวຈະຊອກຫາອີ່ຫຍັງ本日のお仕事終了 しましたpop musicရှေး")
['វេ',
 'វចនានុក្រម',
 'ពហុ',
 'ភាសា',
 'ដោយ',
 'ឥត',
 'គិតថ្លៃ',
 'ฉัน',
 'กินข้าว',
 'ຈະ',
 'ຊອກ',
 'ຫາ',
 'ອີ',
 '່',
 'ຫຍັງ',
 '本日',
 'の',
 'お仕事',
 '終了',
 ' ',
 'しま',
 'した',
 'pop',
 ' ',
 'music',
 'ရှေး']

```

mixed-wordlist.txt and mixed-cluster-rules.txt can be downloaded https://codeberg.org/mekong-lang/wordcut-engine/src/branch/main/data.
